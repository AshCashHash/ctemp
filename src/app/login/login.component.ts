import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';



@Component({
  selector: 'app-assignment1',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
  error = false;
  success = false;
  uerror = false;
  errormssg = '';
  successmssg = '';
  public isAuth = false;
  reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  onKey(event: any) {
    this.error = false;
    this.success = false;
  }

  onLogin() {


    if (this.username === '' || this.password === '') {
      this.error = true;
      this.success = false;
      this.uerror = false;
      this.errormssg = 'Enter The Required Credentials';
    } else if (this.reg.test(this.username)) {
      this.error = false;
      this.success = false;
      this.uerror = false;
      if (this.username === 'ashnish.sharma@zs.com' && this.password === 'abc') {
        this.isAuth = true;
        setTimeout(() => {
          this.isAuth = false;
        }, 1500);
        this.error = false;
        this.success = true;
        this.uerror = false;
        this.successmssg = 'Login Successful';

        var user_obj = {name: 'Ashnish', u_name: this.username, role: 'Intern'};
        localStorage.setItem('user_obj', JSON.stringify(user_obj));

        setTimeout( () => { this.router.navigate(['/site']); }, 2000 );

      } else {
        this.error = true;
        this.success = false;
        this.uerror = false;
        this.errormssg = 'Invalid Credentials';
      }
    } else if (!this.reg.test(this.username)) {
      this.error = true;
      this.success = false;
      this.uerror = true;
      this.errormssg = 'Invalid Username';
    } else {
      this.error = true;
      this.success = false;
      this.uerror = false;
      this.errormssg = 'Invalid Credentials';
    }


  }

  handleEnter(e) {
    if (e.keyCode === 13) {
      this.onLogin();
    }
  }

}
