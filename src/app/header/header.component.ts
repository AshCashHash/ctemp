import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username = 'John Doe';
  name = '';
  role = '';

  constructor() { }

  ngOnInit() {
    var json = localStorage.getItem('user_obj');
    var r_obj = JSON.parse(json);
    this.name = r_obj.name;
    this.username = r_obj.u_name;
    this.role = r_obj.role;
  }

}
