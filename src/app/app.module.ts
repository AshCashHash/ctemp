import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {HeaderComponent} from './header/header.component';
import { SiteComponent } from './site/site.component';
import { ModelComponent } from './model/model.component';


const appRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'site', component: SiteComponent },
  { path: 'model', component: ModelComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    SiteComponent,
    ModelComponent
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    FormsModule,
    RouterModule,
    [RouterModule.forRoot(appRoutes)]

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
