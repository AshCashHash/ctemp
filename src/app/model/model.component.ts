import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {
  username = '';
  name = '';
  role = '';

  constructor() { }

  ngOnInit() {
    var json = localStorage.getItem('user_obj');
    var r_obj = JSON.parse(json);
    this.name = r_obj.name;
    this.username = r_obj.u_name;
    this.role = r_obj.role;
  }

}
